package com.faccigilberthplazamendoza.miprimeraappconversion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText txtFarenge, txtCentigrado ;
    Button convertidor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Log.e("onCreate:", "Plaza Mendoza Gilberth Eugenio");

        convertidor = (Button)findViewById(R.id.btnConvertir);
        txtFarenge = (EditText)findViewById(R.id.txtFarenge);
        txtCentigrado=(EditText)findViewById(R.id.txtCentigrado);

        txtCentigrado.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                float faren;
                faren = (1.8f)*Float.parseFloat(txtCentigrado.getText().toString())+32;
                txtFarenge.setText(""+faren);
                return false;
            }
        });

        txtFarenge.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                float centi;
                centi=(Float.parseFloat(txtFarenge.getText().toString())-32)/(1.8f);
                txtCentigrado.setText(""+centi);
                return false;
            }
        });

    }
}
